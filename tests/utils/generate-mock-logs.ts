import { DeviceMessageLog } from "../../src/model/device-message/DeviceMessageLog";

export function generateMockLogs(from: Date, to: Date, amount: number): DeviceMessageLog[] {
  const increment = Math.floor((to.valueOf() - from.valueOf()) / amount);
  return new Array(amount).fill(null).map((_, index: number) => {
    return DeviceMessageLog.builder()
      .cycleCount(Math.floor(10000 + Math.random() * 80000))
      .enableStatus(Math.random() > 0.5)
      .logId(index + 10)
      .machineId('132')
      .malfunctionData(0)
      .receivedOn(new Date(from.valueOf() + (index + 1) * increment))
      .rpm(200)
      .staffId('122')
      .workId('R20174567890').build();
  });
}
