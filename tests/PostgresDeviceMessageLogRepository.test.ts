import { Pool } from 'pg';
import { expect } from 'chai';
import { format } from 'date-fns';

import { PostgresDeviceMessageLogRepository } from '../src/model/device-message/repository/impl/PostgresDeviceMessageLogRepository';
import { connectPostgresql, createTablesPostgresql } from '../src/config/database';
import { generateMockLogs } from './utils/generate-mock-logs';
import { DeviceMessageLog } from '../src/model/device-message/DeviceMessageLog';

import 'mocha';

describe('Tests on postgresql implementation of repository', () => {
  const DB_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
  const NOW = new Date();

  let repository: PostgresDeviceMessageLogRepository = null;
  let connectionPool: Pool = null;
  let insertedLogId: number = null;

  before(done => {
    connectionPool = connectPostgresql();
    //done();
    createTablesPostgresql(connectionPool).then(
      () => repository = new PostgresDeviceMessageLogRepository(connectionPool)
    ).then(() => done());
  });

  it('Insert log to the repository', done => {
    repository.newLog({
      cycleCount: 10000,
      enableStatus: true,
      machineId: '24',
      malfunctionData: 0,
      receivedOn: NOW,
      rpm: 60,
      staffId: '121',
      workId: 'R20171234567890'
    }).then(logId => {
      expect(typeof logId).to.equal('number');
      insertedLogId = logId;
      done();
    }).catch(e => done(e));
  });

  it('Get specified log with log ID', () => {
    return repository.getLog(insertedLogId).then(log => {
      expect(log.logId).to.equal(insertedLogId);
      expect(log.machineId).to.equal('24');
      expect(log.cycleCount).to.equal(10000);
      expect(log.enableStatus).to.equal(true);
      expect(format(log.receivedOn, DB_DATE_FORMAT)).to.equal(format(NOW, DB_DATE_FORMAT));
      expect(log.malfunctionData).to.equal(0);
      expect(log.workId).to.equal('R20171234567890');
      expect(log.staffId).to.equal('121');
    });
  });

  it('Get logs of a timerange', () => {
    const queryFrom = new Date(2016, 5, 1);
    const queryTo = new Date(2016, 9, 1);
    return new Promise<DeviceMessageLog[]>((resolve, _) => {
      const mockLogs = generateMockLogs(new Date(2016, 0, 1), new Date(2017, 0, 1), 200);
      let logsLeft = mockLogs.length;
      mockLogs.forEach(log => {
        repository.newLog(log).then(logId => {
          log.logId = logId;
          if (--logsLeft === 0) {
            resolve(mockLogs);
          }
        });
      });
    }).then(logs => {
      repository.getLogsBetweenDates(queryFrom, queryTo).then(logs => {
        expect(logs.length).to.lessThan(200);
        logs.forEach(log => {
          expect(log.receivedOn).to.gte(queryFrom).and.lte(queryTo);
        });
      });
    });
  });

  it('Get all logs from repository', () => {
    return repository.getLogs().then(logs => expect(typeof logs).to.equal('object'));
  });

  after(done => {
    // Destroy created tables.
    connectionPool.query('DROP TABLE IF EXISTS devicelog')
      .then(res => connectionPool.end())
      .then(() => done())
      .catch(e => done(e));
  });
});
