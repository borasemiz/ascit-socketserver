import { Server } from 'http';
import { setupDatabase } from './config/database';
import { getDeviceMessageLogRepository, getDeviceClientRepository, getProductRepository } from './config/repository';
import { setupWebSocket } from './config/websocket';
import { setupHttpServer } from './config/httpserver';
import * as settings from './config/settings';
import { Application } from './model/application';

export function bootstrap(): Promise<Server> {
  const application = new Application();
  return setupDatabase()
    .then(connectionPool => {
      application.setDeviceMessageLogRepository(getDeviceMessageLogRepository(connectionPool));
      application.setUserRepository(getDeviceClientRepository(connectionPool));
      application.setProductRepository(getProductRepository(connectionPool));
    })
    .then(() => application.setWebSocket(setupWebSocket()))
    .then(() => setupHttpServer(application));
}