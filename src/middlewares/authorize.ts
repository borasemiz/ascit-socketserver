import { Request, Response, NextFunction } from "express";

import { authorize } from '../services/authorize';

const unauthorized = {
  error: 'Unauthorized',
  code: 403
};

export const authorizationMiddleware = async (request: Request, response: Response, next: NextFunction): Promise<void> => {
  const authHeader = request.header('Authorization');
  try {
    const authorized = await authorize(authHeader);
    if (authorized) {
      next();
    } else {
      response.status(403).json(unauthorized);
    }
  } catch (e) {
    response.status(e.code).json(e);
  }
}
