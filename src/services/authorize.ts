import * as jwt from 'jsonwebtoken';

import { SECRET_KEY } from "../config/settings";

const errorObject = {
  code: 403,
  error: 'Unauthorized'
};

export function verifyToken(token: string): Promise<boolean> {
  return new Promise<boolean>((resolve, reject) => {
    jwt.verify(token, SECRET_KEY, (err, payload: any) => {
      if (err || Date.now() / 1000 >= payload.exp) {
        reject(errorObject);
      } else {
        resolve(true);
      }
    });
  });
}

export async function authorize(authHeader: string): Promise<boolean> {
  if (!authHeader) {
    throw errorObject;
  }

  const [authType, token] = authHeader.split(' ');
  if (!authType || authType.toLowerCase() !== 'bearer') {
    throw errorObject;
  }

  return await verifyToken(token);
}
