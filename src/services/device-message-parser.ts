import { IDeviceMessage } from "../model/device-message/IDeviceMessage";
import { DeviceMessageLog } from "../model/device-message/DeviceMessageLog";

const parser = (message: string): DeviceMessageLog => {
  return new DeviceMessageLog({
    isEnabled: (+message[0]) === 1,
    rpm: +message.slice(1, 3),
    currentStats: {
      horizontalBakla: +message.slice(3, 6),
      verticalBakla: +message.slice(6, 9),
      ballCount: +message.slice(9, 12)
    },
    productId: +message.slice(12, 15),
    targetStats: {
      horizontalBakla: +message.slice(15, 18),
      verticalBakla: +message.slice(18, 21),
      ballCount: +message.slice(21, 24)
    },
    machineId: +message.slice(24, 25),
    malfunctionData: +message[25]
  });
}

export { parser };