DROP TABLE IF EXISTS "devicelog";
DROP TABLE IF EXISTS "user";
DROP TABLE IF EXISTS "product";

CREATE TABLE "user" (
  "id" SERIAL PRIMARY KEY,
  "name" TEXT NOT NULL UNIQUE,
  "password" TEXT NOT NULL
);

CREATE TABLE "product" (
  "product_id" SMALLINT PRIMARY KEY,
  "type" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "weight_kg" FLOAT NOT NULL
);

CREATE TABLE "devicelog" (
  "logid" SERIAL PRIMARY KEY,
  "receivedon" BIGINT NOT NULL,
  "enablestatus" BOOLEAN NOT NULL,
  "rpm" SMALLINT NOT NULL,
  "current_horizontal_bakla" SMALLINT NOT NULL,
  "current_vertical_bakla" SMALLINT NOT NULL,
  "current_ball_count" SMALLINT NOT NULL,
  "target_horizontal_bakla" SMALLINT NOT NULL,
  "target_vertical_bakla" SMALLINT NOT NULL,
  "target_ball_count" SMALLINT NOT NULL,
  "product_id" SMALLINT NOT NULL REFERENCES "product"("product_id"),
  "machine_id" SMALLINT NOT NULL,
  "malfunction_data" SMALLINT NOT NULL
);


DROP INDEX IF EXISTS devicelog_index;
CREATE INDEX devicelog_index ON "devicelog" USING brin (
  "receivedon"
);

INSERT INTO "user" ("name", "password") VALUES ('indet', '$2b$10$Nq2sbN2UlE4dkgYFfTPFru3Jhw2DvWUSQbuZmannjVPwrFrhEbyGO');

INSERT INTO "product" ("product_id", "type", "description", "weight_kg") VALUES
  (101, 'PVC', '50x50x3,00 mm', 1.25), (102, 'PVC', '50x50x3,50 mm', 1.80),
  (103, 'PVC', '50x50x4,00 mm', 2.65), (104, 'PVC', '50x50x4,50 mm', 3.55),
  (105, 'PVC', '40x40x3,00 mm', 1.55),
  (106, 'PVC', '40x40x3,00 mm', 1.55),
  (107, 'PVC', '40x40x3,00 mm', 1.55),
  (108, 'PVC', '40x40x3,00 mm', 1.55),
  (109, 'PVC', '40x40x3,00 mm', 1.55),
  (110, 'PVC', '40x40x3,00 mm', 1.55),
  (111, 'PVC', '40x40x3,00 mm', 1.55),
  (201, 'GLV', '40x40x3,00 mm', 1.55),
  (202, 'GLV', '40x40x3,00 mm', 1.55),
  (203, 'GLV', '40x40x3,00 mm', 1.55),
  (204, 'GLV', '40x40x3,00 mm', 1.55),
  (205, 'GLV', '40x40x3,00 mm', 1.55),
  (206, 'GLV', '40x40x3,00 mm', 1.55),
  (207, 'GLV', '40x40x3,00 mm', 1.55),
  (208, 'GLV', '40x40x3,00 mm', 1.55),
  (209, 'GLV', '40x40x3,00 mm', 1.55),
  (210, 'GLV', '40x40x3,00 mm', 1.55),
  (211, 'GLV', '40x40x3,00 mm', 1.55);
