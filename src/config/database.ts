import * as fs from 'fs';
import * as path from 'path';
import * as process from 'process';
import { Pool, PoolClient, types as pgTypes } from "pg";

import { DatabaseProviders } from './constants';
import * as settings from './settings';


export function createTablesPostgresql(pool: Pool): Promise<void> {
  const query = fs.readFileSync(path.resolve(__dirname, 'query', 'postgresql', 'tables.sql'), 'utf8');
  let _client: PoolClient = null;
  return pool.query(query).then(() => {});
}

export function connectPostgresql(): Pool {
  if (!settings.USER || !settings.PASSWORD || !settings.DATABASE) {
    console.log(process.env['DBUSER'], process.env['DBPASSWORD'], process.env['DBNAME']);
    throw new Error('Database credentials or configuration aren\'t properly provided');
  }
  const pool = new Pool({
    user: settings.USER,
    host: settings.HOST,
    database: settings.DATABASE,
    password: settings.PASSWORD,
    port: settings.PORT,
  });
  return pool;
}

function setupDatabasePostgres(): Promise<Pool> {
  // For returning datetimes as is. Without altering
  pgTypes.setTypeParser(1114, str => str);
  const pool = connectPostgresql();
  if (settings.RECREATE_TABLES) {
    return new Promise((resolve, reject) => {
      createTablesPostgresql(pool)
        .then(() => resolve(pool))
        .catch(e => reject(e));
    });
  } else {
    return new Promise(resolve => resolve(pool));
  }
}

export function setupDatabase(): Promise<any> {
  switch(settings.DB) {
    case DatabaseProviders.POSTGRESQL:
      return setupDatabasePostgres();
  }
  throw new Error('Unknown database provider provided.');
}
