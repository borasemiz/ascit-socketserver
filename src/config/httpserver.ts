import { Server, createServer } from 'http';
import { resolve } from 'path';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as multer from 'multer';

import { Application } from '../model/application';
import {
  publishHandler
} from '../handlers/publish';
import {
  fetchAllLogs
} from '../handlers/logs';
import {
  demoFileUploadHandler,
  demoFileDownloadHandler,
  demoFileUploadInterface,
} from '../handlers/demofile';
import { authorizationMiddleware } from '../middlewares/authorize';
import { authenticateHandler } from '../handlers/auth';
import { getAllProducts } from '../handlers/product';

function setupMiddlewares(expressApp: express.Express): multer.Instance {
  const upload = multer({ dest: resolve(__dirname, 'upload/') });

  expressApp.use(cors());
  expressApp.use(bodyParser.json());
  expressApp.use(bodyParser.text());
  expressApp.use(bodyParser.urlencoded({ extended: true }));

  return upload;
}

function setupHandlers(app: Application, expressApp: express.Express, upload: multer.Instance): void {
  expressApp.post('/auth', authenticateHandler(app));
  expressApp.get('/tokentest', authorizationMiddleware);
  
  expressApp.post('/publish', authorizationMiddleware, publishHandler(app));
  expressApp.get('/logs', authorizationMiddleware, fetchAllLogs(app));
  expressApp.get('/products', authorizationMiddleware, getAllProducts(app));

  expressApp.post('/demofile', upload.single('uploadedFile'), demoFileUploadHandler());
  expressApp.get('/demofile', demoFileUploadInterface());
  expressApp.get('/demofile/:fileName', demoFileDownloadHandler());
  
  /*expressApp.post('/workDescription', authorizationMiddleware, publishWorkDescriptionFactory(db));
  expressApp.get('/workDescription', getWorkDescriptionFactory(db));*/
}

export function setupHttpServer(app: Application): Server {
  const expressApp = express();

  setupHandlers(
    app,
    expressApp,
    setupMiddlewares(expressApp)
  );  

  return createServer(expressApp);
}
