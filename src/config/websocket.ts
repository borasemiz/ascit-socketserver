import * as ws from 'ws';

import { BroadcastingWebSocket, UNKNOWN_CLIENT_ID } from "../model/broadcasting-web-socket";
import * as settings from './settings';
import { verifyToken } from '../services/authorize';

interface ICredentials {
  token: string;
  clientId: string;
}

function authenticateWebSocket(socket: ws): Promise<ICredentials> {
  return new Promise((resolve, reject) => {
    const cancelationTimeout = setTimeout(() => {
      reject('Authentication timeout');
    }, 10000);

    socket.onmessage = async event => {
      try {
        clearTimeout(cancelationTimeout);
        const credentials: ICredentials = JSON.parse(event.data.toString());
        await verifyToken(credentials.token);
        resolve(credentials);
      } catch(e) {
        reject('Authentication failed');
      } finally {
        socket.onmessage = null;
      }
    };
  });
}

export function setupWebSocket(): BroadcastingWebSocket {
  const wss = new ws.Server({ port: settings.WS_PORT });
  const broadcastingWebSocket = new BroadcastingWebSocket(wss);

  wss.on('connection', async (socket: ws) => {
    try {
      await authenticateWebSocket(socket);
      socket.send('OK');
    } catch(e) {
      socket.close(1000, e);
    }
  });
  
  return broadcastingWebSocket;
}
