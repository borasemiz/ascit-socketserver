import * as settings from './settings';
import { DatabaseProviders } from './constants';
import { IDeviceMessageLogRepository } from '../model/device-message/repository/IDeiceMessageLogRepository';
import { DeviceMessageLogRepositoryFactory } from '../model/device-message/repository/DeviceMessageLogRepositoryFactory';
import { IDeviceUserRepository } from '../model/device-user/repository/IDeviceUserRepository';
import { DeviceUserRepositoryFactory } from '../model/device-user/repository/DeviceUserRepositoryFactory';
import { IProductRepository } from '../model/product/repository/IProductRepository';
import { ProductRepositoryFactory } from '../model/product/repository/ProductRepositoryFactory';

export function getDeviceMessageLogRepository(connectionPool: any): IDeviceMessageLogRepository {
  switch (settings.DB) {
    case DatabaseProviders.POSTGRESQL:
      return DeviceMessageLogRepositoryFactory.getPostgresRepository(connectionPool);
  }
  throw new Error('Can\' configure repository unknown database provider.');
}

export function getDeviceClientRepository(connectionPool: any): IDeviceUserRepository {
  switch (settings.DB) {
    case DatabaseProviders.POSTGRESQL:
      return DeviceUserRepositoryFactory.getPostgresRepository(connectionPool);
  }
  throw new Error('Can\' configure repository unknown database provider.');
}

export function getProductRepository(connectionPool: any): IProductRepository {
  switch(settings.DB) {
    case DatabaseProviders.POSTGRESQL:
      return ProductRepositoryFactory.getPostgresRepository(connectionPool);
  }
  throw new Error('Can\' configure repository unknown database provider.');
}
