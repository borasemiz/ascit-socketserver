import * as process from 'process';
import * as path from 'path';
import * as crypto from 'crypto';
import { DatabaseProviders } from './constants';

export const DB = process.env['DB'] || DatabaseProviders.POSTGRESQL;
export const USER = process.env['DBUSER'];
export const PASSWORD = process.env['DBPASSWORD'];
export const DATABASE = process.env['DBNAME'];
export const HOST = process.env['DBHOST'] || 'localhost';
export const PORT: number = parseInt(process.env['PORT']) || 5432;
export const RECREATE_TABLES: boolean = !!process.env['DBRECREATE'] || false;

export const WS_PORT: number = parseInt(process.env['WSPORT']) || 9000;

export const HTTP_PORT: number = parseInt(process.env['HTTP_PORT']) || 8999;

export const UPLOAD_PATH = path.resolve(__dirname, 'upload/');

export const SECRET_KEY = crypto.randomBytes(16); // '206f71fda4169d5d917dea8ff8f63bce'
