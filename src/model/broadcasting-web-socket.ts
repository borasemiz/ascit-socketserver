import * as ws from 'ws';
import { DeviceMessageLog } from './device-message/DeviceMessageLog';

export const UNKNOWN_CLIENT_ID: string = 'unknown';

export class BroadcastingWebSocket {
  public sock: ws.Server;

  constructor(sock: ws.Server) {
    this.sock = sock;
  }

  public broadcast(log: DeviceMessageLog): void {
    this.sock.clients.forEach(client => {
      client.send(JSON.stringify(log));
    });
  }
}
