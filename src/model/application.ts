import { IDeviceMessageLogRepository } from "./device-message/repository/IDeiceMessageLogRepository";
import { BroadcastingWebSocket } from "./broadcasting-web-socket";
import { IDeviceUserRepository } from "./device-user/repository/IDeviceUserRepository";
import { IProductRepository } from "./product/repository/IProductRepository";

export class Application {
  private deviceMessageLogRepository: IDeviceMessageLogRepository;
  private userRepository: IDeviceUserRepository;
  private productRepository: IProductRepository;
  private webSocket: BroadcastingWebSocket;

  public getDeviceMessageLogRepository(): IDeviceMessageLogRepository {
    return this.deviceMessageLogRepository;
  }

  public setDeviceMessageLogRepository(repository: IDeviceMessageLogRepository): void {
    this.deviceMessageLogRepository = repository;
  }

  public getUserRepository(): IDeviceUserRepository {
    return this.userRepository;
  }

  public setUserRepository(repository:IDeviceUserRepository): void {
    this.userRepository = repository;
  }

  public getProductRepository(): IProductRepository {
    return this.productRepository;
  }

  public setProductRepository(repository: IProductRepository): void {
    this.productRepository = repository;
  }

  public setWebSocket(ws: BroadcastingWebSocket): void {
    this.webSocket = ws;
  }

  public getWebSocket(): BroadcastingWebSocket {
    return this.webSocket;
  }
}
