export interface IDeviceMessage {
  isEnabled: boolean;
  rpm: number;
  currentStats: {
    horizontalBakla: number;
    verticalBakla: number;
    ballCount: number;
  };
  targetStats: {
    horizontalBakla: number;
    verticalBakla: number;
    ballCount: number;
  };
  productId: number;
  machineId: number;
  malfunctionData: number;
}
