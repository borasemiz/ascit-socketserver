import { Pool } from 'pg';

import { IDeviceMessageLogRepository } from './IDeiceMessageLogRepository';
import { PostgresDeviceMessageLogRepository } from './impl/PostgresDeviceMessageLogRepository';

export class DeviceMessageLogRepositoryFactory {
  static getPostgresRepository(connectionPool: Pool): IDeviceMessageLogRepository {
    return new PostgresDeviceMessageLogRepository(connectionPool);
  }
}
