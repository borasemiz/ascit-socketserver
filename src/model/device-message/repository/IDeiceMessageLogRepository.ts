import { IDeviceMessage } from "../IDeviceMessage";
import { DeviceMessageLog } from "../DeviceMessageLog";

export interface IDeviceMessageLogRepository {
  newLog(message: DeviceMessageLog): Promise<number>;
  getLog(logId: number): Promise<DeviceMessageLog>;
  getLogs(): Promise<DeviceMessageLog[]>;
  getLogsForMachine(machineId: number): Promise<DeviceMessageLog[]>;
  getLogsForMachineBetweenDates(machineId: number, from: number, to: number): Promise<DeviceMessageLog[]>;
  getLogsBetweenDates(from: number, to: number): Promise<DeviceMessageLog[]>;
}
