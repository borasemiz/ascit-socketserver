import { Pool } from 'pg';
import { format } from 'date-fns';

import { IDeviceMessageLogRepository } from '../IDeiceMessageLogRepository';
import { DeviceMessageLog } from '../../DeviceMessageLog';
import { IDeviceMessage } from '../../IDeviceMessage';

const DB_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

export class PostgresDeviceMessageLogRepository implements IDeviceMessageLogRepository {
  private connection: Pool;
  private tableName: string = 'devicelog';

  constructor(connectionPool: Pool) {
    this.connection = connectionPool;
  }

  public newLog(message: DeviceMessageLog): Promise<number> {
    const query = {
      text: `INSERT INTO ${this.tableName}(receivedon, enablestatus, rpm, current_horizontal_bakla, current_vertical_bakla, current_ball_count, target_horizontal_bakla, target_vertical_bakla, target_ball_count, product_id, machine_id, malfunction_data)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) RETURNING logid`,
      values: [
        message.receivedOn, message.isEnabled, message.rpm,
        message.currentStats.horizontalBakla, message.currentStats.verticalBakla, message.currentStats.ballCount, message.targetStats.horizontalBakla, message.targetStats.verticalBakla, message.targetStats.ballCount,
        message.productId, message.machineId, message.malfunctionData
      ],
    };
    return this.connection.query(query)
      .then(result => {
        return parseInt(result.rows[0]['logid']);
      });
  }

  public getLog(logId: number): Promise<DeviceMessageLog> {
    const query = {
      text: `SELECT * FROM ${this.tableName} WHERE logId = $1`,
      values: [ logId, ],
    };
    return this.connection.query(query)
      .then(result => this.buildDeviceMessageLogFromRow(result.rows[0]));
  }

  public getLogs(): Promise<DeviceMessageLog[]> {
    const query = {
      text: `SELECT * FROM ${this.tableName}`,
    };
    return this.connection.query(query)
      .then(result => result.rows.map(row => this.buildDeviceMessageLogFromRow(row)));
  }

  public getLogsForMachine(machineId: number): Promise<DeviceMessageLog[]> {
    const query = {
      text: `SELECT * FROM ${this.tableName} WHERE machine_id = $1`,
      values: [ machineId ]
    };
    return this.connection.query(query)
      .then(result => result.rows.map(row => this.buildDeviceMessageLogFromRow(row)));
  }

  public getLogsForMachineBetweenDates(machineId: number, from: number, to: number): Promise<DeviceMessageLog[]> {
    const query = {
      text: `SELECT * FROM ${this.tableName} WHERE machineId = $1 AND receivedon BETWEEN $2 AND $3`,
      values: [ machineId, from, to ]
    };
    return this.connection.query(query)
      .then(result => result.rows.map(row => this.buildDeviceMessageLogFromRow(row)));
  }

  public getLogsBetweenDates(from: number, to: number): Promise<DeviceMessageLog[]> {
    const query = {
      text: `SELECT * FROM ${this.tableName} WHERE receivedon BETWEEN $1 AND $2`,
      values: [ from, to ],
    };
    
    return this.connection.query(query)
      .then(result => result.rows.map(row => this.buildDeviceMessageLogFromRow(row)));
  }

  private buildDeviceMessageLogFromRow(row: any): DeviceMessageLog {
    const log = new DeviceMessageLog({
      isEnabled: !!row['enablestatus'],
      rpm: row['rpm'],
      currentStats: {
        horizontalBakla: row['current_horizontal_bakla'],
        verticalBakla: row['current_vertical_bakla'],
        ballCount: row['current_ball_count']
      },
      targetStats: {
        horizontalBakla: row['target_horizontal_bakla'],
        ballCount: row['target_ball_count'],
        verticalBakla: row['target_vertical_bakla']
      },
      productId: row['product_id'],
      machineId: row['machine_id'],
      malfunctionData: row['malfunction_data'],
    });
    log.logId = row['logid'];
    log.receivedOn = row['receivedon'];
    return log;
  }
}
