import { IDeviceMessage } from './IDeviceMessage';

export class DeviceMessageLog implements IDeviceMessage {
  logId: number;
  receivedOn: number;
  isEnabled: boolean;
  rpm: number;
  currentStats: {
    horizontalBakla: number;
    verticalBakla: number;
    ballCount: number;
  };
  targetStats: {
    horizontalBakla: number;
    verticalBakla: number;
    ballCount: number;
  };
  productId: number;
  machineId: number;
  malfunctionData: number;
  
  constructor(deviceMessage?: IDeviceMessage) {
    if (deviceMessage) {
      Object.assign(this, deviceMessage);
    }
  }
}
