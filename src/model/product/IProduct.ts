export interface IProduct {
  productId: number;
  type: string;
  description: string;
  weight: number;
}
