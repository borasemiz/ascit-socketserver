import { IProduct } from "../IProduct";

export interface IProductRepository {
  getProducts(): Promise<IProduct[]>;
}
