import { Pool } from "pg";
import * as bcrypt from "bcrypt";

import { IProduct } from "../../IProduct";
import { IProductRepository } from "../IProductRepository";

export class PostgresProductRepository implements IProductRepository {
  private tableName: string = "product";
  
  constructor(private connectionPool: Pool) {}

  public async getProducts(): Promise<IProduct[]> {
    const query = {
      text: `SELECT * FROM "${this.tableName}"`
    };
    const result = await this.connectionPool.query(query);
    return result.rows.map((row): IProduct => ({
      productId: row['product_id'],
      description: row['description'],
      type: row['type'],
      weight: row['weight_kg']
    }));
  }

}
