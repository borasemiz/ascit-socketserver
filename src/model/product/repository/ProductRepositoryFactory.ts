import { Pool } from "pg";
import { IProductRepository } from "./IProductRepository";
import { PostgresProductRepository } from "./impl/PostgresProductRepository";

export class ProductRepositoryFactory {
  static getPostgresRepository(connectionPool: Pool): IProductRepository {
    return new PostgresProductRepository(connectionPool);
  }
}
