export interface IDeviceUser {
  id: number;
  name: string;
  password: string;
}
