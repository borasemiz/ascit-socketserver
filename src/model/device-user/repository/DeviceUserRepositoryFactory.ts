import { Pool } from "pg";
import { IDeviceUserRepository } from "./IDeviceUserRepository";
import { PostgresDeviceUserRepository } from "./impl/PostgresDeviceUserRepository";

export class DeviceUserRepositoryFactory {
  static getPostgresRepository(connectionPool: Pool): IDeviceUserRepository {
    return new PostgresDeviceUserRepository(connectionPool);
  }
}
