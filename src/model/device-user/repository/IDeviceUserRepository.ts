import { IDeviceUser } from "../IDeviceUser";

export interface IDeviceUserRepository {
  addNewUser(name: string, password: string): Promise<number>;
  getUserById(userId: number): Promise<IDeviceUser>;
  getUserByName(userName: string): Promise<IDeviceUser>;
}
