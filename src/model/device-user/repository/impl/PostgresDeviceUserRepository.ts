import { Pool } from "pg";
import * as bcrypt from "bcrypt";

import { IDeviceUser } from "../../IDeviceUser";
import { IDeviceUserRepository } from "../IDeviceUserRepository";

export class PostgresDeviceUserRepository implements IDeviceUserRepository {
  private tableName: string = "user";
  
  constructor(private connectionPool: Pool) {}

  public async addNewUser(name: string, password: string): Promise<number> {
    const encryptedPassword = await bcrypt.hash(password, 10);
    const query = {
      text: `INSERT INTO "${this.tableName}" ("name", "password") VALUES ($1, $2) RETURNING "id"`,
      values: [ name, encryptedPassword ]
    };
    const result = await this.connectionPool.query(query);
    return parseInt(result.rows[0]['id']);
  }
  
  public async getUserById(userId: number): Promise<IDeviceUser> {
    const query = {
      text: `SELECT * FROM "${this.tableName}" WHERE "id" = $1`,
      values: [ userId ]
    };
    const result = await this.connectionPool.query(query);
    return result.rows.length === 0 ? null : {
      id: result.rows[0]['id'],
      name: result.rows[0]['name'],
      password: result.rows[0]['password']
    };
  }

  public async getUserByName(userName: string): Promise<IDeviceUser> {
    const query = {
      text: `SELECT * FROM "${this.tableName}" WHERE "name" = $1`,
      values: [ userName ]
    };
    const result = await this.connectionPool.query(query);
    return result.rows.length === 0 ? null : {
      id: result.rows[0]['id'],
      name: result.rows[0]['name'],
      password: result.rows[0]['password']
    };
  }
}
