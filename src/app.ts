import { bootstrap } from './bootstrap';
import { HTTP_PORT } from './config/settings';

bootstrap().then(
  server => server.listen(
    HTTP_PORT,
    () => console.log(`Server started on port ${server.address()['port']}`)
  )
);
