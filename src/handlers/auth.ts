import { Request, Response, response } from 'express';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';

import { SECRET_KEY } from '../config/settings';
import { Application } from '../model/application';

export function authenticateHandler(app: Application): (request: Request, response: Response) => Promise<void> {
  return async (request: Request, response: Response): Promise<void> => {
    const repository = app.getUserRepository();
    if (!request.body.username || !request.body.password) {
      response.status(401).json({
        code: 401,
        error: 'Username or password aren\'t provided'
      });
      return;
    }

    const user = await repository.getUserByName(request.body.username);
    if (!user) {
      response.status(401).json({
        code: 401,
        error: 'No such user exists'
      });
      return;
    }
    const passwordMatch = await bcrypt.compare(request.body.password, user.password);
    if (!passwordMatch) {
      response.status(401).json({
        code: 401,
        error: 'Incorrect password'
      });
      return;
    }

    const expiration = (Date.now() / 1000) + 24 * 60 * 60;
    jwt.sign({
      userId: user.id,
      exp: expiration
    }, SECRET_KEY, (err, token) => {
      if (err) {
        response.status(500).json({
          code: 500,
          error: err.message
        });
      } else {
        if (request.query.format && request.query.format === 'json') {
          response.status(200).json({
            token,
            userId: user.id,
            expiration: expiration * 1000
          });
        } else {
          response.header("Content-Type", "text/csv");
          response.status(200).send(`${token},${user.id},${expiration * 1000}`);
        }
      }
    });
  }
}
