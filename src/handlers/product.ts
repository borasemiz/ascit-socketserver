import { Application } from "../model/application";
import { Request, Response } from "express";

export function getAllProducts(app: Application): (req: Request, resp: Response) => Promise<void> {
  return async (req: Request, resp: Response): Promise<void> => {
    try {
      const products = await app.getProductRepository().getProducts();
      resp.status(200).json(products);
    } catch(e) {
      resp.status(500).send(e.toString());
    }
  };
}
