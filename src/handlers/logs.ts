import { Request, Response } from "express";

import { Application } from "../model/application";

export const fetchAllLogs = (app: Application): (req: Request, res: Response) => void => {
  return async (request: Request, response: Response): Promise<void> => {
    try {
      const logs = await app.getDeviceMessageLogRepository().getLogs();
      response.status(200).json(logs);
    } catch (e) {
      response.status(500).json({
        error: 'Internal Server Error',
        code: 500
      });
    }
  };
}
