import { Request, Response } from "express";

import { parser } from '../services/device-message-parser';
import { Application } from "../model/application";

export const publishHandler = (app: Application): (req: Request, res: Response) => void => {
  return async (request: Request, response: Response): Promise<void> => {
    if (typeof request.body === 'object') {
      return;
    } else if (typeof request.body === 'string') {
      const deviceMessage = parser(request.body);
      deviceMessage.receivedOn = Date.now();
      deviceMessage.logId = await app.getDeviceMessageLogRepository().newLog(deviceMessage);
      app.getWebSocket().broadcast(deviceMessage);
    }
    response.status(200).send('OK');
  }
}
