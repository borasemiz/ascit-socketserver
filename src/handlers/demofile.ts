import { Request, Response } from 'express';
import * as fs from 'fs';
import { resolve } from 'path';
import { UPLOAD_PATH } from '../config/settings';

const HTML_TEMPLATE = `
<!DOCTYPE html>
<html>
<head>
  <title>Upload</title>
</head>
<body>
  <form method="POST" enctype="multipart/form-data" action="/demofile">
    <label for="upload">
      Lutfen dosya seciniz
    </label>
    <input type="file" id="upload" name="uploadedFile">
    <button type="submit">Tamamla</button>
  </form>
</body>
</html>
`;

export const demoFileUploadHandler = () => {
  return (req: Request, res: Response): void => {
    fs.renameSync(
      resolve(UPLOAD_PATH, req.file.filename),
      resolve(UPLOAD_PATH, req.file.originalname)
    );
    res.status(200).send('OK');
  };
}

export const demoFileDownloadHandler = () => {
  return (req: Request, res: Response): void => {
    const fileName: string = req.params.fileName;
    if (fs.existsSync(resolve(UPLOAD_PATH, fileName))) {
      res.status(200).sendFile(resolve(UPLOAD_PATH, fileName));
      return;
    }
    res.status(404).send();
  };
}

export const demoFileUploadInterface = () => {
  return (req: Request, res: Response): void => {
    res.type('html').status(200).send(HTML_TEMPLATE);
  };
};
